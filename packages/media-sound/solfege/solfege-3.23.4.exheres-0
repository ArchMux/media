# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ alpha=true suffix=tar.gz ]
require python [ blacklist=2 multibuild=false python_opts="[sqlite]" ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Ear training program written to help you train intervals, chords,
scales and rhythms"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-text/docbook-xsl-ns-stylesheets
        app-text/txt2man
        dev-lang/swig
        dev-libs/libxslt
        dev-util/itstool
        media-sound/lilypond[>=2.12]
        sys-apps/texinfo
        sys-devel/gettext
    run:
        dev-python/pyalsa[python_abis:*(-)?]
        dev-python/pycairo[python_abis:*(-)?]
        gnome-bindings/pygobject:3[>=3.2.2][python_abis:*(-)?]
        media-sound/timidity++
        x11-libs/gtk+:3[>=3.4.0][gobject-introspection]
"

# Wants to access a wayland compositor
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-cross.patch
    "${FILES}"/${PNV}-Generate-documentation-with-itstool-instead-of-depre.patch
    "${FILES}"/${PNV}-Prevent-internet-access-by-xsltproc.patch
    "${FILES}"/${PNV}-Fix-webbrowser-registration-issue-with-python-3.7.2.patch
    "${FILES}"/${PNV}-Fix-invisible-menubar-text.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-oss-sound
    --enable-docbook-stylesheet=/usr/share/sgml/docbook/xsl-ns-stylesheets
)

DEFAULT_SRC_COMPILE_PARAMS=( skipmanual=yes )
DEFAULT_SRC_INSTALL_PARAMS=( skipmanual=yes )

src_prepare() {
    # Don't run xmllit
    edo sed -e "s|test: xmllint po/solfege.pot|test: po/solfege.pot|" \
        -i Makefile.in

    autotools_src_prepare
}


# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xsane-0.995.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A graphical scanning frontend"
HOMEPAGE="http://www.${PN}.org"
DOWNLOADS="mirror://gentoo/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gimp [[ description = [ Build a plugin for the GIMP ] ]]
    lcms
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: ca cs da de es fi fr hu it ja nl pa pl pt pt_BR ro ru sk sl sr sv tr vi zh zh_CN )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        media-gfx/sane-backends
        media-libs/libpng
        x11-libs/gtk+:2
        gimp? ( media-gfx/gimp[>=2] )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.995-close-fds.patch
    "${FILES}"/${PN}-0.995-xdg-open.patch
    "${FILES}"/${PN}-0.997-ipv6.patch
    "${FILES}"/${PN}-0.998-desktop-file.patch
    "${FILES}"/${PN}-0.998-libpng-1.5.patch
    "${FILES}"/${PN}-0.998-preview-selection.patch
    "${FILES}"/${PN}-0.998-wmclass.patch
    "${FILES}"/${PN}-0.999-lcms2.patch
    "${FILES}"/${PN}-0.999-man-page.patch
    "${FILES}"/${PN}-0.999-no-file-selected.patch
    "${FILES}"/${PN}-0.999-pdf-no-high-bpp.patch
    "${FILES}"/${PN}-0.999-coverity.patch
    "${FILES}"/${PN}-0.999-signal-handling.patch
    "${FILES}"/${PN}-0.999-snprintf-update.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --datadir=/usr/share
    --disable-sanetest
    --disable-gimptest
    --enable-jpeg
    --enable-nls
    --enable-png
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gimp lcms tiff )

AT_M4DIR=( m4 )

src_prepare() {
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:g" m4/gtk-2.0.m4 m4/gimp-2.0.m4
    edo sed -i -e "/AC_PATH_PROG(PKG_CONFIG/d" m4/gtk-2.0.m4 m4/gimp-2.0.m4
    edo sed -i -e "/AR/s:ar:$(exhost --tool-prefix)&:" intl/Makefile.in
    edo sed -i -e "s:ar:$(exhost --tool-prefix)&:" lib/Makefile.in

    # fix cross/multiarch locale install path
    edo sed \
        -e 's:gnulocaledir = $(prefix)/share/locale:gnulocaledir = /usr/share/locale:g' \
        -i po/Makefile.in

    edo touch config.sub
    edo chmod +x config.sub
    autotools_src_prepare
}

src_install() {
    default

    # remove empty directory
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    if option gimp ; then
        # using `gimptool-2.0 --gimpplugindir` is not cross-friendly
        local plugindir=$(${PKG_CONFIG} --variable=gimplibdir gimp-2.0)/plug-ins

        dodir "${plugindir}"
        dosym /usr/$(exhost --target)/bin/${PN} "${plugindir}"
    fi
}

